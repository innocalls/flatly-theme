<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>
        @section('title')
            {{ Setting::get('core::site-name') }}
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="stylesheet" href="{{ asset('themes/material/vendor/plugins/bootstrap/css/bootstrap.min.css') }}">
    @if(locale()=='ar')
        <link rel="stylesheet" href="{{ asset('themes/material/css/rtl_style.css') }}">
    @else
        <link rel="stylesheet" href="{{ asset('themes/material/css/style.css') }}">
    @endif

</head>
<body class="hold-transition login-page">

<section id="wrapper">
    @yield('content')
</section>

<!-- Bootstrap -->
<script src="{{ asset('themes/material/vendor/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('themes/material/vendor/plugins/popper/popper.min.js') }}"></script>
<script src="{{ asset('themes/material/vendor/plugins/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('themes/material/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('themes/material/js/html5shiv.min.js')}}"></script>
<script src="{{ asset('themes/material/js/respond.min.js')}}"></script>
<script src="{{ asset('themes/material/js/sidebarmenu.js')}}"></script>
<script src="{{ asset('themes/material/vendor/plugins/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
<script src="{{ asset('themes/material/js/vendor/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{ asset('themes/material/js/custom.min.js')}}"></script>

</body>
</html>
